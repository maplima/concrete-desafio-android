package br.com.concretesolutions.githubjavapop.service;

import com.google.gson.Gson;

class GitHubError extends Exception {

    private String message;

    @Override
    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    static GitHubError fromJson(String s) {
        return new Gson().fromJson(s, GitHubError.class);
    }

    @Override
    public String toString() {
        return new Gson().toJson(this);
    }
}
