package br.com.concretesolutions.githubjavapop.view.activity;

import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import br.com.concretesolutions.githubjavapop.R;
import br.com.concretesolutions.githubjavapop.adapter.PullAdapter;
import br.com.concretesolutions.githubjavapop.databinding.ActivityPullsBinding;
import br.com.concretesolutions.githubjavapop.model.Pull;
import br.com.concretesolutions.githubjavapop.model.Repository;
import br.com.concretesolutions.githubjavapop.service.GitHubRepository.OnResponseListener;
import br.com.concretesolutions.githubjavapop.viewModel.PullsViewModel;

public class PullsActivity extends BaseActivity {
    private static final String EXTRA_REPOSITORY = "EXTRA_REPOSITORY";
    private static final String LIST_STATE_KEY = "LIST_STATE";

    private ActivityPullsBinding binding;
    private PullsViewModel pullsViewModel;
    private Parcelable listState;

    public static Intent newIntent(Context context, Repository repository) {
        Intent intent = new Intent(context, PullsActivity.class);
        intent.putExtra(EXTRA_REPOSITORY, repository);
        return intent;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = DataBindingUtil.setContentView(this, R.layout.activity_pulls);
        pullsViewModel = new PullsViewModel(this, new PullAdapter(), new ArrayList<>(), onResponse());
        binding.setViewModel(pullsViewModel);

        setSupportActionBar(binding.toolbar);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

        Repository repository = getIntent().getParcelableExtra(EXTRA_REPOSITORY);
        setTitle(repository.getName());
        pullsViewModel.loadPulls(repository.getUser().getLogin(), repository.getName());
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        outState.putParcelable(LIST_STATE_KEY, binding.pulls.getLayoutManager().onSaveInstanceState());
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);

        if (savedInstanceState != null) {
            listState = savedInstanceState.getParcelable(LIST_STATE_KEY);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (listState != null) {
            binding.pulls.getLayoutManager().onRestoreInstanceState(listState);
        }
    }

    public OnResponseListener<List<Pull>> onResponse(){
        return (response, error) -> {
            if (error != null) {
                Toast.makeText(getContext(), error.getMessage(), Toast.LENGTH_LONG).show();
                return;
            }

            PullAdapter adapter = (PullAdapter) binding.pulls.getAdapter();
            adapter.addAll(response);
            setPullsCount(response);
        };
    }

    private void setPullsCount(List<Pull> pulls) {
        int openedCount = 0;
        int closedCount = 0;

        for (Pull pull : pulls) {
            switch (pull.getState()) {
                case "open":
                    openedCount++;
                    break;
                case "closed":
                    closedCount++;
                    break;
            }
        }

        int gold = ContextCompat.getColor(this, R.color.gold);
        SpannableString opened = new SpannableString(openedCount + " opened");
        opened.setSpan(new ForegroundColorSpan(gold), 0, opened.length(), 0);

        binding.pullsCount.setText(opened);
        binding.pullsCount.append(" / ");
        binding.pullsCount.append(closedCount + " closed");
    }
}
