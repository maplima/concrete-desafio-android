package br.com.concretesolutions.githubjavapop.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.common.base.Objects;
import com.google.gson.annotations.SerializedName;

public class Repository implements Parcelable {
    private long id;
    private String name;
    private String description;
    @SerializedName("forks_count")
    private int forks;
    @SerializedName("stargazers_count")
    private int stars;
    @SerializedName("owner")
    private User user;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getForks() {
        return forks;
    }

    public void setForks(int forks) {
        this.forks = forks;
    }

    public int getStars() {
        return stars;
    }

    public void setStars(int stars) {
        this.stars = stars;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Repository() {
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(this.id);
        dest.writeString(this.name);
        dest.writeString(this.description);
        dest.writeInt(this.forks);
        dest.writeInt(this.stars);
        dest.writeParcelable(this.user, flags);
    }

    public Repository(long id, String name, String description, int forks, int stars, User user) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.forks = forks;
        this.stars = stars;
        this.user = user;
    }

    protected Repository(Parcel in) {
        this.id = in.readLong();
        this.name = in.readString();
        this.description = in.readString();
        this.forks = in.readInt();
        this.stars = in.readInt();
        this.user = in.readParcelable(User.class.getClassLoader());
    }

    public static final Creator<Repository> CREATOR = new Creator<Repository>() {
        public Repository createFromParcel(Parcel source) {
            return new Repository(source);
        }

        public Repository[] newArray(int size) {
            return new Repository[size];
        }
    };

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (!(o instanceof Repository)) return false;

        Repository repository = (Repository) o;

        return Objects.equal(id, repository.id) &&
                Objects.equal(name, repository.name) &&
                Objects.equal(description, repository.description) &&
                Objects.equal(forks, repository.forks) &&
                Objects.equal(stars, repository.stars) &&
                Objects.equal(user, repository.user);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id, name, description, forks, stars, user);
    }
}
