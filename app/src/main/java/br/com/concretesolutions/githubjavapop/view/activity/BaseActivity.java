package br.com.concretesolutions.githubjavapop.view.activity;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;

public abstract class BaseActivity extends AppCompatActivity {

    protected Context getContext() {
        return this;
    }
}
