package br.com.concretesolutions.githubjavapop;

import android.app.Application;
import android.content.Context;

import java.io.File;

import br.com.concretesolutions.githubjavapop.service.GitHubService;

public class AppApplication extends Application {

    private GitHubService gitHubService;

    public static AppApplication get(Context context) {
        return (AppApplication) context.getApplicationContext();
    }

    public GitHubService getGitHubService() {
        if (gitHubService == null) {
            gitHubService = GitHubService.Factory.create(new File(getCacheDir(), "http"));
        }

        return gitHubService;
    }
}
