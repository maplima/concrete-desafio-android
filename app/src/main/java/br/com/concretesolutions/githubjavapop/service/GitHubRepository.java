package br.com.concretesolutions.githubjavapop.service;

import android.support.annotation.Nullable;
import android.util.Log;

import java.io.IOException;
import java.util.List;

import br.com.concretesolutions.githubjavapop.model.Pull;
import br.com.concretesolutions.githubjavapop.model.Repository;
import br.com.concretesolutions.githubjavapop.model.Resume;
import br.com.concretesolutions.githubjavapop.model.User;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class GitHubRepository {

    private GitHubService gitHubService;

    public GitHubRepository(GitHubService gitHubService) {
        this.gitHubService = gitHubService;
    }

    public void loadRepositories(String TAG, String query, String sort, int page, OnResponseListener<List<Repository>> listener) {
        Call<Resume<Repository>> call = gitHubService.publicRepositories(query, sort, page);

        call.enqueue(new Callback<Resume<Repository>>() {
            @Override
            public void onResponse(Call<Resume<Repository>> call, Response<Resume<Repository>> response) {
                if (response.isSuccessful()) {
                    if (listener != null && response.body() != null) {
                        listener.onResponse(response.body().getItems(), null);
                    }
                } else {
                    Log.e(TAG, "Error loading GitHub repositories");
                }
            }

            @Override
            public void onFailure(Call<Resume<Repository>> call, Throwable error) {
                Log.e(TAG, "Error loading GitHub repositories ", error);
            }
        });
    }

    public void loadPulls(String TAG, String login, String name, OnResponseListener<List<Pull>> listener) {
        Call<List<Pull>> call = gitHubService.pullRequests(login, name);

        call.enqueue(new Callback<List<Pull>>() {
            @Override
            public void onResponse(Call<List<Pull>> call, Response<List<Pull>> response) {
                if (response.isSuccessful()) {
                    if (listener != null) {
                        listener.onResponse(response.body(), null);
                    }
                } else {
                    try {
                        listener.onResponse(null, GitHubError.fromJson(response.errorBody().string()));
                    } catch (IOException e) {
                        Log.e(TAG, "Error loading GitHub pulls ", e);
                    }
                }
            }

            @Override
            public void onFailure(Call<List<Pull>> call, Throwable error) {
                Log.e(TAG, "Error loading GitHub pulls ", error);
            }
        });
    }

    public void loadUserName(String TAG, String url, OnResponseListener<String> listener) {
        Call<User> call = gitHubService.userFromUrl(url);

        call.enqueue(new Callback<User>() {
            @Override
            public void onResponse(Call<User> call, Response<User> response) {
                if (response.isSuccessful()) {
                    if (listener != null && response.body() != null) {
                        listener.onResponse(response.body().getName(), null);
                    }
                } else {
                    Log.e(TAG, "Error loading GitHub repositories");
                }
            }

            @Override
            public void onFailure(Call<User> call, Throwable error) {
                Log.e(TAG, "Error loading GitHub repositories ", error);
            }
        });
    }

    public interface OnResponseListener<T> {
        void onResponse(@Nullable T response, @Nullable Throwable error);
    }
}
