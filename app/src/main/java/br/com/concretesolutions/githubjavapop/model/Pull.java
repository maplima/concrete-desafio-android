package br.com.concretesolutions.githubjavapop.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.common.base.Objects;
import com.google.gson.annotations.SerializedName;

public class Pull implements Parcelable {
    private long id;
    private String title;
    private String body;
    private String state;
    private User user;
    @SerializedName("html_url")
    private String url;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Pull() {
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(this.id);
        dest.writeString(this.title);
        dest.writeString(this.body);
        dest.writeParcelable(this.user, flags);
        dest.writeString(this.url);
    }

    public Pull(long id, String title, String body, User user, String url) {
        this.id = id;
        this.title = title;
        this.body = body;
        this.user = user;
        this.url = url;
    }

    protected Pull(Parcel in) {
        this.id = in.readLong();
        this.title = in.readString();
        this.body = in.readString();
        this.user = in.readParcelable(User.class.getClassLoader());
        this.url = in.readString();
    }

    public static final Parcelable.Creator<Pull> CREATOR = new Parcelable.Creator<Pull>() {
        public Pull createFromParcel(Parcel source) {
            return new Pull(source);
        }

        public Pull[] newArray(int size) {
            return new Pull[size];
        }
    };

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (!(o instanceof Pull)) return false;

        Pull pull = (Pull) o;

        return Objects.equal(id, pull.id) &&
                Objects.equal(title, pull.title) &&
                Objects.equal(body, pull.body) &&
                Objects.equal(user, pull.user) &&
                Objects.equal(url, pull.url);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id, title, body, user, url);
    }
}
