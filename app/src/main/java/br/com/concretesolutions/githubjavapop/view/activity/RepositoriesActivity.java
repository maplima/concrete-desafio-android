package br.com.concretesolutions.githubjavapop.view.activity;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import br.com.concretesolutions.githubjavapop.R;
import br.com.concretesolutions.githubjavapop.adapter.RepositoryAdapter;
import br.com.concretesolutions.githubjavapop.databinding.ActivityRepositoriesBinding;
import br.com.concretesolutions.githubjavapop.model.Repository;
import br.com.concretesolutions.githubjavapop.service.GitHubRepository.OnResponseListener;
import br.com.concretesolutions.githubjavapop.view.EndlessRecyclerViewScrollListener;
import br.com.concretesolutions.githubjavapop.viewModel.RepositoriesViewModel;

public class RepositoriesActivity extends BaseActivity {
    private static final String LIST_STATE_KEY = "LIST_STATE";

    private ActivityRepositoriesBinding binding;
    private RepositoriesViewModel repositoriesViewModel;
    private Parcelable listState;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = DataBindingUtil.setContentView(this, R.layout.activity_repositories);
        repositoriesViewModel = new RepositoriesViewModel(this, new RepositoryAdapter(), new ArrayList<>(), onResponse());
        binding.setViewModel(repositoriesViewModel);

        setSupportActionBar(binding.toolbar);

        loadRepositories();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        outState.putParcelable(LIST_STATE_KEY, binding.repositories.getLayoutManager().onSaveInstanceState());
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);

        if (savedInstanceState != null) {
            listState = savedInstanceState.getParcelable(LIST_STATE_KEY);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (listState != null) {
            binding.repositories.getLayoutManager().onRestoreInstanceState(listState);
        }
    }

    private void loadRepositories() {
        LinearLayoutManager layoutManager = (LinearLayoutManager) binding.repositories.getLayoutManager();
        binding.repositories.addOnScrollListener(new EndlessRecyclerViewScrollListener(layoutManager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {
                repositoriesViewModel.loadRepositories("language:java", "stars", page);
            }
        });

        repositoriesViewModel.loadRepositories("language:java", "stars", 0);
    }

    public OnResponseListener<List<Repository>> onResponse() {
        return (response, error) -> {
            RepositoryAdapter adapter = (RepositoryAdapter) binding.repositories.getAdapter();
            adapter.addAll(response);
        };
    }
}
