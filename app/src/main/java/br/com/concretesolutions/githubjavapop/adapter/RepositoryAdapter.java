package br.com.concretesolutions.githubjavapop.adapter;

import com.squareup.picasso.Picasso;

import br.com.concretesolutions.githubjavapop.R;
import br.com.concretesolutions.githubjavapop.adapter.viewHolder.DataBindingViewHolder;
import br.com.concretesolutions.githubjavapop.databinding.RowRepositoryBinding;
import br.com.concretesolutions.githubjavapop.model.Repository;
import br.com.concretesolutions.githubjavapop.viewModel.RepositoryViewModel;

public class RepositoryAdapter extends BaseDataBindingAdapter<Repository, RowRepositoryBinding> {

    @Override
    protected int getLayout() {
        return R.layout.row_repository;
    }

    @Override
    public void onBindViewHolder(DataBindingViewHolder<RowRepositoryBinding> holder, int position) {
        Repository repository = list.get(position);
        holder.binding.setViewModel(new RepositoryViewModel(holder.getContext(), repository));

        Picasso.with(holder.binding.photoUser.getContext())
                .load(repository.getUser().getAvatarUrl())
                .placeholder(R.drawable.placeholder)
                .error(R.drawable.placeholder)
                .into(holder.binding.photoUser);
    }
}
