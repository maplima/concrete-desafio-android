package br.com.concretesolutions.githubjavapop.viewModel;

import android.content.Context;

import java.util.List;

import br.com.concretesolutions.githubjavapop.AppApplication;
import br.com.concretesolutions.githubjavapop.adapter.RepositoryAdapter;
import br.com.concretesolutions.githubjavapop.model.Repository;
import br.com.concretesolutions.githubjavapop.service.GitHubRepository;
import br.com.concretesolutions.githubjavapop.service.GitHubRepository.OnResponseListener;

public class RepositoriesViewModel {
    private static final String TAG = "RepositoriesViewModel";

    private Context context;
    private RepositoryAdapter repositoryAdapter;
    private List<Repository> repositories;
    private OnResponseListener<List<Repository>> listener;

    public RepositoriesViewModel(Context context, RepositoryAdapter repositoryAdapter, List<Repository> repositories, OnResponseListener<List<Repository>> listener) {
        this.context = context;
        this.repositoryAdapter = repositoryAdapter;
        this.repositories = repositories;
        this.listener = listener;
    }

    public RepositoryAdapter getAdapter() {
        repositoryAdapter.setList(repositories);
        return repositoryAdapter;
    }

    public void loadRepositories(String query, String sort, int page) {
        new GitHubRepository(AppApplication.get(context).getGitHubService()).loadRepositories(TAG, query, sort, page, listener);
    }
}