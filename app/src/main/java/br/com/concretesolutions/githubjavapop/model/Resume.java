package br.com.concretesolutions.githubjavapop.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Resume<T> {
    @SerializedName("total_count")
    private long totalCount;
    @SerializedName("incomplete_results")
    private boolean incompleteResults;
    private List<T> items;

    public List<T> getItems() {
        return items;
    }

    public void setItems(List<T> items) {
        this.items = items;
    }
}
