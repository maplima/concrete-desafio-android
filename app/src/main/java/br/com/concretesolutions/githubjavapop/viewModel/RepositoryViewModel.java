package br.com.concretesolutions.githubjavapop.viewModel;

import android.content.Context;
import android.databinding.ObservableField;
import android.view.View;

import br.com.concretesolutions.githubjavapop.AppApplication;
import br.com.concretesolutions.githubjavapop.model.Repository;
import br.com.concretesolutions.githubjavapop.service.GitHubRepository;
import br.com.concretesolutions.githubjavapop.service.GitHubRepository.OnResponseListener;
import br.com.concretesolutions.githubjavapop.view.activity.PullsActivity;

public class RepositoryViewModel {
    private static final String TAG = "RepositoryViewModel";

    private Context context;
    private Repository repository;
    public ObservableField<String> userName;

    public RepositoryViewModel(Context context, Repository repository) {
        this.context = context;
        this.repository = repository;
        this.userName = new ObservableField<>();
        new GitHubRepository(AppApplication.get(context).getGitHubService()).loadUserName(TAG, repository.getUser().getUrl(), onResponse());
    }

    public void onItemClick(View view) {
        context.startActivity(PullsActivity.newIntent(context, repository));
    }

    public Repository getRepository() {
        return repository;
    }

    public void setRepository(Repository repository) {
        this.repository = repository;
    }

    public String getStars() {
        return String.valueOf(repository.getStars());
    }

    public String getForks() {
        return String.valueOf(repository.getForks());
    }

    private OnResponseListener<String> onResponse() {
        return (response, error) -> {
            repository.getUser().setName(response);
            userName.set(repository.getUser().getName());
        };
    }
}