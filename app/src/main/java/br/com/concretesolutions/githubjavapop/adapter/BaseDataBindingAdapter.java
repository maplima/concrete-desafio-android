package br.com.concretesolutions.githubjavapop.adapter;

import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import br.com.concretesolutions.githubjavapop.adapter.viewHolder.DataBindingViewHolder;

public abstract class BaseDataBindingAdapter<M, B extends ViewDataBinding> extends RecyclerView.Adapter<DataBindingViewHolder<B>> {

    protected List<M> list = new ArrayList<>();

    @Override
    public DataBindingViewHolder<B> onCreateViewHolder(ViewGroup parent, int viewType) {
        B binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), getLayout(), parent, false);
        return new DataBindingViewHolder<>(binding);
    }

    protected abstract int getLayout();

    @Override
    public void onBindViewHolder(DataBindingViewHolder<B> holder, int position) {
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public void setList(List<M> list) {
        this.list = list;
    }

    public void addAll(final List<M> objects) {
        this.list.addAll(objects);
        notifyDataSetChanged();
    }
}
