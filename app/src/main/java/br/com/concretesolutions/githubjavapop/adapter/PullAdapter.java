package br.com.concretesolutions.githubjavapop.adapter;

import com.squareup.picasso.Picasso;

import br.com.concretesolutions.githubjavapop.R;
import br.com.concretesolutions.githubjavapop.adapter.viewHolder.DataBindingViewHolder;
import br.com.concretesolutions.githubjavapop.databinding.RowPullBinding;
import br.com.concretesolutions.githubjavapop.model.Pull;
import br.com.concretesolutions.githubjavapop.viewModel.PullViewModel;

public class PullAdapter extends BaseDataBindingAdapter<Pull, RowPullBinding> {
    @Override
    protected int getLayout() {
        return R.layout.row_pull;
    }

    @Override
    public void onBindViewHolder(DataBindingViewHolder<RowPullBinding> holder, int position) {
        Pull pull = list.get(position);
        holder.binding.setViewModel(new PullViewModel(holder.getContext(), pull));

        Picasso.with(holder.binding.photoUser.getContext())
                .load(pull.getUser().getAvatarUrl())
                .placeholder(R.drawable.placeholder)
                .error(R.drawable.placeholder)
                .into(holder.binding.photoUser);
    }
}
