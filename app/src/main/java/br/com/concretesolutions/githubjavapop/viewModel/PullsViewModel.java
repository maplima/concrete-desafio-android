package br.com.concretesolutions.githubjavapop.viewModel;

import android.content.Context;

import java.util.List;

import br.com.concretesolutions.githubjavapop.AppApplication;
import br.com.concretesolutions.githubjavapop.adapter.PullAdapter;
import br.com.concretesolutions.githubjavapop.model.Pull;
import br.com.concretesolutions.githubjavapop.service.GitHubRepository;
import br.com.concretesolutions.githubjavapop.service.GitHubRepository.OnResponseListener;

public class PullsViewModel {
    private static final String TAG = "PullsViewModel";

    private Context context;
    private PullAdapter pullAdapter;
    private List<Pull> pulls;
    private OnResponseListener<List<Pull>> listener;

    public PullsViewModel(Context context, PullAdapter pullAdapter, List<Pull> pulls, OnResponseListener<List<Pull>> listener) {
        this.context = context;
        this.pullAdapter = pullAdapter;
        this.pulls = pulls;
        this.listener = listener;
    }

    public PullAdapter getAdapter() {
        pullAdapter.setList(pulls);
        return pullAdapter;
    }

    public void loadPulls(String login, String name) {
        new GitHubRepository(AppApplication.get(context).getGitHubService()).loadPulls(TAG, login, name, listener);
    }
}
