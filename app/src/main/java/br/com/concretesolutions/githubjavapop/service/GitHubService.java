package br.com.concretesolutions.githubjavapop.service;

import java.io.File;
import java.util.List;
import java.util.concurrent.TimeUnit;

import br.com.concretesolutions.githubjavapop.BuildConfig;
import br.com.concretesolutions.githubjavapop.model.Pull;
import br.com.concretesolutions.githubjavapop.model.Repository;
import br.com.concretesolutions.githubjavapop.model.Resume;
import br.com.concretesolutions.githubjavapop.model.User;
import okhttp3.Cache;
import okhttp3.CacheControl;
import okhttp3.OkHttpClient;
import okhttp3.OkHttpClient.Builder;
import okhttp3.Request;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;
import retrofit2.http.Url;

public interface GitHubService {

    @GET("/search/repositories")
    Call<Resume<Repository>> publicRepositories(@Query("q") String query,
                                                @Query("sort") String sort,
                                                @Query("page") int page);

    @GET
    Call<User> userFromUrl(@Url String userUrl);

    @GET("repos/{login}/{name}/pulls")
    Call<List<Pull>> pullRequests(@Path("login") String login,
                                  @Path("name") String fullName);

    class Factory {
        private static final int CACHE_SIZE = 30 * 1024 * 1024; //30MB
        private static final int TIMEOUT = 30; //30 segundos

        public static GitHubService create(File directory) {
            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl("https://api.github.com/")
                    .client(createHttpClient(directory))
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();

            return retrofit.create(GitHubService.class);
        }

        private static OkHttpClient createHttpClient(File directory) {
            Builder builder = new Builder();
            logging(builder);
            cache(builder, directory);
            builder.connectTimeout(TIMEOUT, TimeUnit.SECONDS);

            return builder.build();
        }

        private static void logging(Builder builder) {
            if (BuildConfig.DEBUG) {
                HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
                interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
                builder.addInterceptor(interceptor);
            }
        }

        private static void cache(Builder builder, File directory) {
            if (directory != null) {
                Cache cache = new Cache(directory, CACHE_SIZE);
                builder.cache(cache);
                builder.addInterceptor(chain -> {
                    Request request = chain.request();
                    request = new Request.Builder()
                            .cacheControl(new CacheControl.Builder()
                                    .maxAge(1, TimeUnit.DAYS)
                                    .minFresh(4, TimeUnit.HOURS)
                                    .maxStale(8, TimeUnit.HOURS)
                                    .build())
                            .url(request.url())
                            .build();

                    return chain.proceed(request);
                });
            }
        }
    }
}
