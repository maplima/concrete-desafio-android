package br.com.concretesolutions.githubjavapop.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.common.base.Objects;
import com.google.gson.annotations.SerializedName;

public class User implements Parcelable {
    private long id;
    private String login;
    private String name;
    private String url;
    @SerializedName("avatar_url")
    private String avatarUrl;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getAvatarUrl() {
        return avatarUrl;
    }

    public void setAvatarUrl(String avatarUrl) {
        this.avatarUrl = avatarUrl;
    }

    public User() {
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(this.id);
        dest.writeString(this.login);
        dest.writeString(this.url);
        dest.writeString(this.avatarUrl);
    }

    public User(long id, String login, String name, String avatarUrl) {
        this.id = id;
        this.login = login;
        this.name = name;
        this.avatarUrl = avatarUrl;
    }

    protected User(Parcel in) {
        this.id = in.readLong();
        this.login = in.readString();
        this.name = in.readString();
        this.url = in.readString();
        this.avatarUrl = in.readString();
    }

    public static final Creator<User> CREATOR = new Creator<User>() {
        public User createFromParcel(Parcel source) {
            return new User(source);
        }

        public User[] newArray(int size) {
            return new User[size];
        }
    };

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (!(o instanceof User)) return false;

        User user = (User) o;

        return Objects.equal(id, user.id) &&
                Objects.equal(login, user.login) &&
                Objects.equal(name, user.name) &&
                Objects.equal(url, user.url) &&
                Objects.equal(avatarUrl, user.avatarUrl);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id, login, name, url, avatarUrl);
    }
}