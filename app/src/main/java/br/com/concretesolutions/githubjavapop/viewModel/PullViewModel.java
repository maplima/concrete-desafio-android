package br.com.concretesolutions.githubjavapop.viewModel;

import android.content.Context;
import android.content.Intent;
import android.databinding.ObservableField;
import android.net.Uri;
import android.view.View;

import br.com.concretesolutions.githubjavapop.AppApplication;
import br.com.concretesolutions.githubjavapop.model.Pull;
import br.com.concretesolutions.githubjavapop.service.GitHubRepository;
import br.com.concretesolutions.githubjavapop.service.GitHubRepository.OnResponseListener;

public class PullViewModel {
    private static final String TAG = "PullViewModel";

    private Context context;
    private Pull pull;
    public ObservableField<String> userName;

    public PullViewModel(Context context, Pull pull) {
        this.context = context;
        this.pull = pull;
        this.userName = new ObservableField<>();
        new GitHubRepository(AppApplication.get(context).getGitHubService()).loadUserName(TAG, pull.getUser().getUrl(), onResponse());
    }

    public void onItemClick(View view) {
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setData(Uri.parse(pull.getUrl()));
        context.startActivity(intent);
    }

    public Pull getPull() {
        return pull;
    }

    public void setPull(Pull pull) {
        this.pull = pull;
    }

    private OnResponseListener<String> onResponse() {
        return (response, error) -> {
            pull.getUser().setName(response);
            userName.set(pull.getUser().getName());
        };
    }
}
